:syntax on

" NerdTree
set nocompatible
filetype off

if has('vim_starting')
	set runtimepath+=~/.vim/bundle/neobundle.vim
	call neobundle#begin(expand('~/.vim/bundle/'))
endif

"insert here your Neobundle plugins"
NeoBundle 'scrooloose/nerdtree'
NeoBundle 'soramugi/auto-ctags.vim'
NeoBundle 'leafgarland/typescript-vim'

call neobundle#end()

filetype plugin indent on

nnoremap <silent><C-e> :NERDTreeToggle<CR>

" タブを表示するときの幅
set tabstop=4
" タブを挿入するときの幅
set shiftwidth=4
" タブをタブとして扱う(スペースに展開しない)
" set noexpandtab
" タブはスペースとして扱う
set expandtab
" 
set softtabstop=0

" 自動インデント
set autoindent

" 行番号を表示
set number

" タブに番号を表示.
if exists("+showtabline")
	function! MyTabLine()
		let s = ''
		let wn = ''
		let t = tabpagenr()
		let i = 1
		while i <= tabpagenr('$')
			let buflist = tabpagebuflist(i)
			let winnr = tabpagewinnr(i)
			let s .= '%' . i . 'T'
			let s .= (i == t ? '%1*' : '%2*')
			let s .= ' '
			let wn = tabpagewinnr(i,'$')

			let s .= '%#TabNum#'
			let s .= i
			" let s .= '%*'
			let s .= (i == t ? '%#TabLineSel#' : '%#TabLine#')
			let bufnr = buflist[winnr - 1]
			let file = bufname(bufnr)
			let buftype = getbufvar(bufnr, 'buftype')
			if buftype == 'nofile'
				if file =~ '\/.'
					let file = substitute(file, '.*\/\ze.', '', '')
				endif
			else
				let file = fnamemodify(file, ':p:t')
			endif
			if file == ''
				let file = '[No Name]'
			endif
			let s .= ' ' . file . ' '
			let i = i + 1
		endwhile
		let s .= '%T%#TabLineFill#%='
		let s .= (tabpagenr('$') > 1 ? '%999XX' : 'X')
		return s
	endfunction
	set stal=2
	set tabline=%!MyTabLine()
	set showtabline=1
	highlight link TabNum Special
endif

" カラースキームを変更
colorscheme molokai

" カラースキームを調整
hi Normal ctermbg=0
hi LineNr ctermbg=233

" vim-sessionの自動読み込みを無効化
:let g:session_autoload = 'no'

" autocomplete辞書ファイルの登録
autocmd FileType php :set dictionary=~/.vim/dict/php.dict

set backspace=indent,eol,start

" auto ctags
let g:auto_ctags = 1

" ctags jump in new tab
nnoremap <C-j> :tab split<CR> :exe("tjump ".expand('<cword>'))<CR>

" カーソル行ハイライト
set cursorline
" カーソル列ハイライト
set cursorcolumn
" 一度にタブで開くファイル数の上限
set tabpagemax=50
" ペースト時に選択範囲をコピーしない
xnoremap p pgvyx
